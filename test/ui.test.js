const puppeteer = require('puppeteer');
const url = 'http://localhost:3000'
const openBrowser = true;

beforeAll(async () => {
    browser = await puppeteer.launch({
        headless: !openBrowser,
        devtools: true
    });
    page = await browser.newPage();
    await page.goto(url);
});

afterAll(async () => {
    browser.close();
});

//1 - example
test('[Content] Login page title should exist', async () => {
    let title = await page.$eval('body > div > form > div:nth-child(1) > h3', (content) => content.innerHTML);
    expect(title).toBe('Login');
});

// 2
test('[Content] Login page join button should exist', async () => {
    let but = await page.$eval('body > div > form > div:nth-child(4) > button', (content) => content.innerHTML);
    expect(but).toBe('Join');
})

//3 - example
test('[Screenshot] Login page', async () => {
    await page.screenshot({ path: 'test/screenshots/login.png' });
})

// 4
test('[Screenshot] Welcome message', async () => {
    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', { delay: 100 });
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'homeworksuck', { delay: 100 });
    await page.keyboard.press('Enter', { delay: 100 });

    await page.waitFor(1000);
    await page.screenshot({ path: 'test/screenshots/welcome.png' });
    await page.close();
})

// 5
test('[Screenshot] Error message when you login without user and room name', async () => {
    page = await browser.newPage();
    await page.goto(url);
    await page.keyboard.press('Enter', { delay: 100 });

    await page.waitFor(1000);
    await page.screenshot({ path: 'test/screenshots/error.png' });
})

//6
test('[Content] Welcome message', async () => {
    await page.waitFor(1000);
    await page.type('body > div.centered-form__form > form > div:nth-child(2) > input[type=text]', 'John', { delay: 100 });
    await page.type('body > div.centered-form__form > form > div:nth-child(3) > input[type=text]', 'homeworksuck', { delay: 100 });
    await page.keyboard.press('Enter', { delay: 100 });
    await page.waitForSelector('#users > ol > li');
    let welcome = await page.$eval('body > div:nth-child(2) > ol > li > div:nth-child(2) > p', (content) => content.innerHTML);
    expect(welcome).toBe('Hi John, Welcome to the chat app');
    await page.close();
})

// 7 - example
test('[Behavior] Type user name', async () => {
    page = await browser.newPage();
    await page.goto(url);
    await page.type('body > div.centered-form__form > form > div:nth-child(2) > input[type=text]', 'John', { delay: 100 });
    let userName = await page.evaluate(() => {
        return document.querySelector('body > div.centered-form__form > form > div:nth-child(2) > input[type=text]').value;
    });
    expect(userName).toBe('John');
    await page.close();
})

// 8
test('[Behavior] Type room name', async () => {
    page = await browser.newPage();
    await page.goto(url);
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'homeworksuck', 100);
    let roomName = await page.evaluate(() => {
        return document.querySelector('body > div > form > div:nth-child(3) > input[type=text]').value;
    });
    expect(roomName).toBe('homeworksuck');
    await page.close();
})

// 9 - example
test('[Behavior] Login', async () => {
    page = await browser.newPage();
    await page.goto(url);
    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', { delay: 100 });
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'homeworksuck', { delay: 100 });
    await page.keyboard.press('Enter', { delay: 100 });

    await page.waitForSelector('#users > ol > li');
    let member = await page.evaluate(() => {
        return document.querySelector('#users > ol > li').innerHTML;
    });

    expect(member).toBe('John');
})

// 10
test('[Behavior] Login 2 users', async () => {
    let member = await page.evaluate(() => {
        return document.querySelector('#users > ol > li').innerHTML;
    });
    expect(member).toBe('John');

    page2 = await browser.newPage();
    await page2.goto(url);

    await page2.type('body > div > form > div:nth-child(2) > input[type=text]', 'Mike', { delay: 100 });
    await page2.type('body > div > form > div:nth-child(3) > input[type=text]', 'homeworksuck', { delay: 100 });
    await page2.keyboard.press('Enter', { delay: 100 });
    await page2.waitForSelector('#users > ol > li');
    let member2 = await page.evaluate(() => {
        return document.querySelector('#users > ol > li:nth-child(2)').innerHTML;
    });
    expect(member2).toBe('Mike');
})

//11
test('[Content] The "Send" button should exist', async () => {
    let but = await page.$eval('body >div:nth-child(2) > div > form > button', (content) => content.innerHTML);
    expect(but).toBe('Send');
})

// 12
test('[Behavior] Send a message', async () => {
    await page.type('body > div:nth-child(2) > div > form > input[type=text]', 'Homework is pretty hard', { delay: 100 });
    await page.keyboard.press('Enter', { delay: 100 });
    let message = await page.$eval('#messages > li:nth-child(3) > div.message__body > p', (content) => content.innerHTML);
    expect(message).toBe('Homework is pretty hard');
})

// 13
test('[Behavior] John says "Hi" and Mike says "Hello"', async () => {
    await page.type('body > div:nth-child(2) > div > form > input[type=text]', 'Hi', { delay: 100 });
    await page.keyboard.press('Enter', { delay: 100 });

    await page2.type('body > div:nth-child(2) > div > form > input[type=text]', 'Hellow', { delay: 100 });
    await page2.keyboard.press('Enter', { delay: 100 });

    let JohnSay = await page2.$eval('#messages > li:nth-child(3) > div.message__body > p', (content) => content.innerHTML);
    expect(JohnSay).toBe('Hi');
    let MikeSay = await page2.$eval('#messages > li:nth-child(4) > div.message__body > p', (content) => content.innerHTML);
    expect(MikeSay).toBe('Hellow');
})

// 14
test('[Content] The "Send location" button should exist', async () => {
    let but = await page.$eval('#send-location', (content) => content.innerHTML);
    expect(but).toBe('Send location');
})

// 15
test('[Behavior] Send a location message', async () => {
    await page2.click('#send-location');
    let but2 = await page.$eval('#send-location', (content) => content.innerHTML);
    expect(but2).toBe('Send location');
})